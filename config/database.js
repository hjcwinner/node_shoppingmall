const mongoose = require('mongoose');



// database 연결

const db_Options = {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify : false,
    useCreateIndex : true
}


mongoose
    .connect(process.env.MONGODB_URI, db_Options)
    .then(() => console.log("mongdb connected ..."))
    .catch(err => console.log(err.message));