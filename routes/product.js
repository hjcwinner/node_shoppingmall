//1번
const express = require("express");
const router = express.Router();

const checkAuth = require('../middleware/checkAuth')

const {
    products_get_all,
    products_register_product,
    products_get_detail,
    products_update_product,
    products_delete_product
} = require('../controllers/product')


//3
// 전체 product 불러오기
router.get("/total", products_get_all);

//상세 product 불러오기
router.get("/:productid", checkAuth, products_get_detail)


// product 등록하기
router.post("/posting", checkAuth , products_register_product);


//5
// product 수정하기
router.put("/update/:productid", checkAuth, products_update_product);
//6
// product 삭제하기
router.delete("/:productid", checkAuth, products_delete_product);




//2번
module.exports = router;