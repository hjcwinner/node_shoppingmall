const express = require('express');
const router = express.Router();

const {
    user_register,
    user_login
} = require('../controllers/user')


// 회원가입 api
router.post("/register", user_register);


// 로그인 api
router.post("/login", user_login)



module.exports = router;
