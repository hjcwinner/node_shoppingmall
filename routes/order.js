//1 express불러오고 라우터 메소드 부른다
const express = require("express");
const router = express.Router();

const checkAuth = require('../middleware/checkAuth') 

const {
    orders_get_all,
    orders_get_detail,
    orders_register_order,
    orders_patch_order,
    orders_delete_order
} = require('../controllers/order')

//불러오기
router.get("/", checkAuth, orders_get_all);

//1개불러오기
router.get("/:orderid", checkAuth, orders_get_detail)


//등록하기
router.post("/", checkAuth, orders_register_order);

//수정하기
router.patch("/:orderid", checkAuth, orders_patch_order);

//삭제하기
router.delete("/:orderid", checkAuth, orders_delete_order);

//라우터를 모듈화시켜서 보낸다
module.exports = router;

