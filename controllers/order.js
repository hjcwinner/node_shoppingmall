

const orderModel = require('../model/order');



exports.orders_get_all = (req, res) => {
    orderModel
        .find()
        .populate("product", ["name", "price"])
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.product,
                        quantity : doc.quantity,
                        request : {
                            type: "GET",
                            url: "http://localhost:7777/order/" + doc._id 
                        }
                    }
                }) 
            }
            res.json(response)
        })
        .catch(err => {
            res.json({
                err : err.message
            })
        })
}

exports.orders_get_detail = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findById(id)
        .then(doc => {
            if(!doc){
                res.json({
                    message : "상품없음"
                })
            }
            else
            {
                res.json({
                    message : id + " 이상품 주문",
                    orderInfo : {
                        id : doc._id,
                        product : doc.product,
                        quantity : doc.quantity,
                        request : {
                            type : "GET",
                            url : "http://localhost:7777/order/"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_register_order = (req, res) => {
    const {product, quantity} = req.body;
    const order = new orderModel({
        product, quantity
    })

    order
        .save()
        .then(doc => {
            res.json({
                message : "order product data",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    qauntity : doc.quantity,
                    request : {
                        type : "GET",
                        url : "http://localhost:7777/order/" + doc._id
                    }

                }
            });
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        })


}

exports.orders_patch_order = (req,res) => {
    const id = req.params.orderid
    const updateOps = {}
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    orderModel
        .findByIdAndUpdate(id, { $set : updateOps })
        .then(() => {
            res.json({
                message : "update",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/order/" + id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_delete_order = (req,res) => {
    const id = req.params.orderid
    orderModel
     .findByIdAndDelete(id) 
     .then(() => {
         res.json({
             message : "선택한주문삭제",
             request : {
                 type : "GET",
                 url : "http://localhost:7777/order/"
             }
         })
     })
     .catch(err =>{
         res.json({
             message : err.message
         })
     })
 }