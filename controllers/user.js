const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userModel = require('../model/user')



exports.user_register = (req, res) => {

    // email 유무 체크 => 패스워드 암호화 => 회원가입(저장)
    userModel
        .findOne({email : req.body.cemail})
        .then(user => {
            if(user) {
                return res.json({
                    message : "가입된 이메일이 있습니다."
                })
            }
            else {
                // 패스워드 암호화
                bcrypt.hash(req.body.cpassword, 10, (err, hash) => {
                    if(err){
                        return res.json({
                            message : err.message
                        })
                    }
                    else{
                        // 회원가입
                        const newUser = new userModel({
                            username : req.body.cusername,
                            email : req.body.cemail,
                            password : hash
                        })

                        newUser
                            .save()
                            .then(doc => {
                                res.json({
                                    id : doc._id,
                                    username : doc.username,
                                    email : doc.email,
                                    password : doc.password,  
                                })
                            })
                            .catch(err => {
                                res.json({
                                    message : err.message
                                })
                            })
                        }
                    })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })  
}

exports.user_login = (req, res) => {
    // email유무 체크 => password매칭 => 로그인
    userModel
        .findOne({email : req.body.cemail})
        .then(user => {
            if(!user) {
                return res.json({
                    message : "no email"
                })
            }
            else {
                bcrypt.compare(req.body.cpassword, user.password, (err, isMatch) =>{
                    console.log(isMatch)
                    if(err || isMatch === false) {
                        return res.json({
                            message : "password incorrect"
                        })
                    }
                    else {
                        const token = jwt.sign(
                            {userid : user._id},
                            process.env.SECRET_KEY,
                            { expiresIn : 360000 }
                        )
                    
                        res.json({
                            message : "successful login",
                            tokenInfo : token
                        })

                    }
                })

                // console.log(user)
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}