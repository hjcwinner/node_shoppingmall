

const productModel = require('../model/product');


exports.products_get_all = (req, res) => {

    productModel
        .find()
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type: "GET",
                            url: "http://localhost:7777/product/" + doc._id 
                        }
                    }
                })
            }

            res.json(response)
            // res.json({
            //     message : "get total product",
            //     count : docs.length, 
            //     products : docs
            // })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });




    // res.json({
    //     message : 'product get'
    // });
}


exports.products_register_product = (req, res) => {


    const {name, price} = req.body;

    const product = new productModel({
        name, price
    });


    product
        .save()
        .then(doc => {
            res.json({
                message : "saved product data", 
                productInfo : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : {
                        type : "GET",
                        url : "http://localhost:7777/product/" + doc._id
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
    // res.json({
    //     message : 'product post',
    //     productInfo : product
    // });
}


exports.products_get_detail = (req, res) => {

    const id = req.params.productid

    productModel
        .findById(id)
        .then(doc => {
            
            if(!doc){
                res.json({
                    message : "no product"
                })
            }
            else
            {
                res.json({
                    
                    message : id + "    reqparamsproductid",
                    productinfo : {
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "GET",
                            url : "http://localhost:7777/product/" + id
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
}


exports.products_update_product = (req,res) => {

    const id = req.params.productid

    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }


    productModel
        .findByIdAndUpdate(id, { $set: updateOps})
        .then(() => {
            res.json({
                message : "update product",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/product/" + id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}


exports.products_delete_product = (req,res) => {
    // res.json({
    //     message : 'product delete'
    // });
    productModel
        .findByIdAndDelete(req.params.productid)
        .then(() => {
            res.json({
                message : "deleted product",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/product/"
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
}