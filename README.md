## 주요기능
--------------------
* MEN(MongoDB - ExpressJS - NodeJS)
* CRUD(create,read,update,delete) 구축
* Database 보안설정 및 Database 코드리펙토링
* 회원가입시 bcryptjs를 이용한 password 암호화
* 로그인시 email형식체크 및 중복체크하기
* 로그인 완료시 token 발행 / 발행된 토큰 검증하기
* mongoDB을 이용한 DATA 관리
* 로그인 완료시 jsonwebtoken 발행
* code 리펙토리 및 code 정리


## 활용한기술
---------------------
* expressJS, MongoDB, Postman
* body-parser, morgan, nodemon, mongoose, dotenv
* jsonwebtoken, bcryptjs, passport

## 스터디노트
--------------------
***1. 빌딩 성공후 화면***
- 빌드에 성공하면 아래와 같은 화면(port number & MongoDB connect)이 나온다.


![2020-10-16_16_38_25](/uploads/d9f209253b27c9b734e4289bd3eb225b/2020-10-16_16_38_25.PNG)

**1. 쇼핑몰 CRUD 제작[Product]**
~~~
const productModel = require('../model/product');

exports.products_get_all = (req, res) => {

    productModel
        .find()
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type: "GET",
                            url: "http://localhost:7777/product/" + doc._id 
                        }
                    }
                })
            }

            res.json(response)
            // res.json({
            //     message : "get total product",
            //     count : docs.length, 
            //     products : docs
            // })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
}

exports.products_register_product = (req, res) => {

    const {name, price} = req.body;

    const product = new productModel({
        name, price
    });

    product
        .save()
        .then(doc => {
            res.json({
                message : "saved product data", 
                productInfo : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : {
                        type : "GET",
                        url : "http://localhost:7777/product/" + doc._id
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
    // res.json({
    //     message : 'product post',
    //     productInfo : product
    // });
}

exports.products_get_detail = (req, res) => {

    const id = req.params.productid

    productModel
        .findById(id)
        .then(doc => {
            
            if(!doc){
                res.json({
                    message : "no product"
                })
            }
            else
            {
                res.json({
                    
                    message : id + "reqparamsproductid",
                    productinfo : {
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "GET",
                            url : "http://localhost:7777/product/" + id
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
}

exports.products_update_product = (req,res) => {

    const id = req.params.productid

    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }


    productModel
        .findByIdAndUpdate(id, { $set: updateOps})
        .then(() => {
            res.json({
                message : "update product",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/product/" + id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_delete_product = (req,res) => {
    // res.json({
    //     message : 'product delete'
    // });
    productModel
        .findByIdAndDelete(req.params.productid)
        .then(() => {
            res.json({
                message : "deleted product",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/product/"
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        });
}
~~~

**2. 쇼핑몰 CRUD 제작[Order]**
~~~
const orderModel = require('../model/order');

exports.orders_get_all = (req, res) => {
    orderModel
        .find()
        .populate("product", ["name", "price"])
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.product,
                        quantity : doc.quantity,
                        request : {
                            type: "GET",
                            url: "http://localhost:7777/order/" + doc._id 
                        }
                    }
                }) 
            }
            res.json(response)
        })
        .catch(err => {
            res.json({
                err : err.message
            })
        })
}

exports.orders_get_detail = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findById(id)
        .then(doc => {
            if(!doc){
                res.json({
                    message : "상품없음"
                })
            }
            else
            {
                res.json({
                    message : id + " 이상품 주문",
                    orderInfo : {
                        id : doc._id,
                        product : doc.product,
                        quantity : doc.quantity,
                        request : {
                            type : "GET",
                            url : "http://localhost:7777/order/"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_register_order = (req, res) => {
    const {product, quantity} = req.body;
    const order = new orderModel({
        product, quantity
    })

    order
        .save()
        .then(doc => {
            res.json({
                message : "order product data",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    qauntity : doc.quantity,
                    request : {
                        type : "GET",
                        url : "http://localhost:7777/order/" + doc._id
                    }

                }
            });
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        })
}

exports.orders_patch_order = (req,res) => {
    const id = req.params.orderid
    const updateOps = {}
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    orderModel
        .findByIdAndUpdate(id, { $set : updateOps })
        .then(() => {
            res.json({
                message : "update",
                request : {
                    type : "GET",
                    url : "http://localhost:7777/order/" + id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_delete_order = (req,res) => {
    const id = req.params.orderid
    orderModel
     .findByIdAndDelete(id) 
     .then(() => {
         res.json({
             message : "선택한주문삭제",
             request : {
                 type : "GET",
                 url : "http://localhost:7777/order/"
             }
         })
     })
     .catch(err =>{
         res.json({
             message : err.message
         })
     })
 }
~~~


**3. 로그인시 Json-web-token을 이용한 token 발행**
~~~
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userModel = require('../model/user')

exports.user_register = (req, res) => {

    // email 유무 체크 => 패스워드 암호화 => 회원가입(저장)
    userModel
        .findOne({email : req.body.cemail})
        .then(user => {
            if(user) {
                return res.json({
                    message : "가입된 이메일이 있습니다."
                })
            }
            else {
                // 패스워드 암호화
                bcrypt.hash(req.body.cpassword, 10, (err, hash) => {
                    if(err){
                        return res.json({
                            message : err.message
                        })
                    }
                    else{
                        // 회원가입
                        const newUser = new userModel({
                            username : req.body.cusername,
                            email : req.body.cemail,
                            password : hash
                        })

                        newUser
                            .save()
                            .then(doc => {
                                res.json({
                                    id : doc._id,
                                    username : doc.username,
                                    email : doc.email,
                                    password : doc.password,  
                                })
                            })
                            .catch(err => {
                                res.json({
                                    message : err.message
                                })
                            })
                        }
                    })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })  
}

exports.user_login = (req, res) => {
    // email유무 체크 => password매칭 => 로그인
    userModel
        .findOne({email : req.body.cemail})
        .then(user => {
            if(!user) {
                return res.json({
                    message : "no email"
                })
            }
            else {
                bcrypt.compare(req.body.cpassword, user.password, (err, isMatch) =>{
                    console.log(isMatch)
                    if(err || isMatch === false) {
                        return res.json({
                            message : "password incorrect"
                        })
                    }
                    else {
                        const token = jwt.sign(
                            {userid : user._id},
                            process.env.SECRET_KEY,
                            { expiresIn : 360000 }
                        )
                    
                        res.json({
                            message : "successful login",
                            tokenInfo : token
                        })

                    }
                })

                // console.log(user)
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}
~~~

## TO DO LIST
✔︎ 현시점 기준 reactjs 공부 진행중[영화lab, netflex clone 완성]

✔︎ react native 앱 개발예정

✔︎ 풀스택으로 개발예정

✔︎ MySQL 데이터베이스 변경
