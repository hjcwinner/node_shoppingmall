

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const dotEnv = require('dotenv');
dotEnv.config();

const app = express();

const productRoutes = require('./routes/product');
const userRoutes = require('./routes/user');
const orderRoutes = require('./routes/order');

//database연결
require("./config/database");



// middlewar
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));



// 라우팅e
app.use('/product', productRoutes);
app.use('/order', orderRoutes);
app.use('/user', userRoutes);
const port = process.env.PORT;


app.listen(port, console.log("서버시작됨"));










